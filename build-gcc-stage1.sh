#!/bin/bash

./checkenv.sh || { exit 1; }

TARGET=$1

OSVER=$(uname)
## Apple needs to pretend to be linux
if [ ${OSVER:0:6} == Darwin ]; then
	TARG_XTRA_OPTS="--build=i386-linux-gnu --host=i386-linux-gnu"
else
	TARG_XTRA_OPTS=""
fi

## Determine the maximum number of processes that Make can work with.
PROC_NR=$(getconf _NPROCESSORS_ONLN)

## Create and enter the toolchain/build directory
mkdir -p toolchain/build && cd toolchain/build || { exit 1; }

## Create and enter the build directory.
rm -rf gcc-$TARGET-stage1 && mkdir gcc-$TARGET-stage1 && cd gcc-$TARGET-stage1 || { exit 1; }

## Configure the build.
../../$TARGET/gcc/configure \
  --prefix="$PSPDEV" \
  --target="$TARGET" \
  --enable-languages="c" \
  --enable-lto \
  --with-newlib \
  --without-headers \
  --disable-libssp \
  $TARG_XTRA_OPTS || { exit 1; }

## Compile and install.
make -j $PROC_NR clean   || { exit 1; }
make -j $PROC_NR all     || { exit 1; }
make -j $PROC_NR install || { exit 1; }
make -j $PROC_NR clean   || { exit 1; }

## Exit the build directory
cd .. || { exit 1; }

## Exit the toolchain/build directory
cd ../.. || { exit 1; }

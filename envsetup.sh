#!/bin/bash

if [ -z "$PSPDEV_ROOT" ]; then
    # if no root defined, then set local PSPDEV_ROOT
    # to enable multiple development environments
    export PSPDEV_ROOT=$PWD/pspdev
fi

if [ -n "$PSPDEV" ]; then
    if [ "$PSPDEV" == "$PSPDEV_ROOT" ]; then
        echo "PSPDEV environment already set to $PSPDEV"
        return 0
    else
        echo "ERROR: PSPDEV environment set to $PSPDEV"
        echo "                   -> instead of $PSPDEV_ROOT"
        return 1
    fi
fi

mkdir -p pspdev

# Create needed vars
export PSPDEV=$PSPDEV_ROOT
export PSPSDK=$PSPDEV/psp/sdk
export PSPSDKSRC=$PWD/libs/pspsdk

# Add binaries to PATH
export PATH=$PATH:$PSPDEV/bin
export PATH=$PATH:$PSPDEV/psp/bin
export PATH=$PATH:$PSPSDK/bin

echo "PSPDEV environment set to $PSPDEV"
return 0

#!/bin/bash

./checkenv.sh || { exit 1; }

TARGET=$1

## Determine the maximum number of processes that Make can work with.
PROC_NR=$(getconf _NPROCESSORS_ONLN)

## Create and enter the toolchain/build directory
mkdir -p toolchain/build && cd toolchain/build || { exit 1; }

## Create and enter the build directory.
rm -rf newlib-$TARGET && mkdir newlib-$TARGET && cd newlib-$TARGET || { exit 1; }

## Configure the build.
../../"$TARGET"/newlib/configure \
  --prefix="$PSPDEV" \
  --target="$TARGET" \
  --enable-newlib-iconv \
  --enable-newlib-multithread \
  --enable-newlib-mb || { exit 1; }

## Compile and install.
make -j $PROC_NR clean   || { exit 1; }
make -j $PROC_NR all     || { exit 1; }
make -j $PROC_NR install || { exit 1; }
make -j $PROC_NR clean   || { exit 1; }

## Exit the build directory
cd .. || { exit 1; }

## Exit the toolchain/build directory
cd ../.. || { exit 1; }

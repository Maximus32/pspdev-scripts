#!/bin/bash

./checkenv.sh || { exit 1; }

## NOTE: Building a new toolchain will also wipe the $PS2DEV folder
rm -rf $PSPDEV/*
rm -rf toolchain/build

./build-binutils.sh    psp || { exit 1; }
./build-gcc-prepare.sh psp || { exit 1; }
./build-gcc-stage1.sh  psp || { exit 1; }

cd libs/pspsdk
./bootstrap
./configure --with-pspdev="$PSPDEV"
make install-data || { exit 1; }
cd ../..

./build-newlib.sh     psp || { exit 1; }
./build-gcc-stage2.sh psp || { exit 1; }

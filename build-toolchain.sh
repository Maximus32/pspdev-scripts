#!/bin/bash

./checkenv.sh || { exit 1; }

## NOTE: Building a new toolchain will also wipe the $PSPDEV folder
rm -rf $PSPDEV/*

cd toolchain/psptoolchain

./toolchain.sh 1 || { exit 1; }
./toolchain.sh 2 || { exit 1; }
./toolchain.sh 3 || { exit 1; }
./toolchain.sh 4 || { exit 1; }
./toolchain.sh 5 || { exit 1; }

cd ../..

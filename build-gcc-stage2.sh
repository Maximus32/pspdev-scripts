#!/bin/bash

./checkenv.sh || { exit 1; }

TARGET=$1

OSVER=$(uname)
## Apple needs to pretend to be linux
if [ ${OSVER:0:6} == Darwin ]; then
	TARG_XTRA_OPTS="--build=i386-linux-gnu --host=i386-linux-gnu --enable-cxx-flags=-G0"
else
	TARG_XTRA_OPTS="--enable-cxx-flags=-G0"
fi

## Determine the maximum number of processes that Make can work with.
PROC_NR=$(getconf _NPROCESSORS_ONLN)

## Create and enter the toolchain/build directory
mkdir -p toolchain/build && cd toolchain/build || { exit 1; }

## Create and enter the build directory.
rm -rf gcc-$TARGET-stage2 && mkdir gcc-$TARGET-stage2 && cd gcc-$TARGET-stage2 || { exit 1; }

## Configure the build.
../../$TARGET/gcc/configure \
  --prefix="$PSPDEV" \
  --target="$TARGET" \
  --enable-languages="c,c++" \
  --enable-lto \
  --with-newlib \
  $TARG_XTRA_OPTS || { exit 1; }

## Compile and install.
make -j $PROC_NR clean   || { exit 1; }
make -j $PROC_NR all     || { exit 1; }
make -j $PROC_NR install || { exit 1; }
make -j $PROC_NR clean   || { exit 1; }

## Exit the build directory
cd .. || { exit 1; }

## Exit the toolchain/build directory
cd ../.. || { exit 1; }

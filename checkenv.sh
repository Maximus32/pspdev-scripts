#!/bin/bash

if [ -z "$PSPDEV" ]; then
  echo "ERROR: PSPDEV environment not set!!!"
  echo "execute \"source envsetup.sh\" first"
  exit 1
fi

if [ "$PSPDEV" != "$PSPDEV_ROOT" ]; then
  echo "ERROR: Wrong environment!!!"
  echo "    PSPDEV      = $PSPDEV"
  echo "    PSPDEV_ROOT = $PSPDEV_ROOT"
  exit 1
fi

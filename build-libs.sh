#!/bin/bash
CMAKE_OPTIONS="-Wno-dev -DCMAKE_TOOLCHAIN_FILE=$PSPSDK/pspdev.cmake -DCMAKE_INSTALL_PREFIX=$PSPSDK/ports -DBUILD_SHARED_LIBS=OFF "
#CMAKE_OPTIONS+="-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON "

function build {
    mkdir $1
    cd $1
    cmake $CMAKE_OPTIONS $3 ../../$2 || { exit 1; }
    make clean all || { exit 1; }
    make install || { exit 1; }
    cd ..
}

./checkenv.sh || { exit 1; }

## NOTE: Building all libraries will also wipe the $PS2SDK folder
rm -rf $PSPSDK

cd libs

##
## Build ps2sdk first
##
cd pspsdk
make clean all install || { exit 1; }
cd ..

## gcc needs to include libpspuser from pspsdk to be able to build executables.
ln -sf "$PSPSDK/lib/libpspuser.a" "$PSPDEV/psp/lib/libpspuser.a" || { exit 1; }

## Add pspdev.cmake
cp ../pspdev.cmake $PSPSDK/ || { exit 1; }

##
## Build cmake projects
##
rm -rf build
mkdir build
cd build
build zlib      zlib
build libpng    libpng "-DPNG_SHARED=OFF -DPNG_STATIC=ON"
build freetype  freetype2
build libconfig libconfig "-DBUILD_EXAMPLES=OFF -DBUILD_TESTS=OFF"
cd ..

cd ..

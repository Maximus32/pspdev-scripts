#!/bin/bash

TARGET=$1

# set gcc version
#GCC_VERSION=4.9.3
GMP_VERSION=5.1.3
MPC_VERSION=1.0.2
MPFR_VERSION=3.1.2

cd toolchain/$TARGET/

# Get GMP, MPC and MPFR sources
wget -nc https://ftp.gnu.org/gnu/gmp/gmp-$GMP_VERSION.tar.bz2    || { exit 1; }
wget -nc https://ftp.gnu.org/gnu/mpc/mpc-$MPC_VERSION.tar.gz     || { exit 1; }
wget -nc https://ftp.gnu.org/gnu/mpfr/mpfr-$MPFR_VERSION.tar.bz2 || { exit 1; }

# Extract sources
rm -rf gmp-$GMP_VERSION   && tar xfj gmp-$GMP_VERSION.tar.bz2   || { exit 1; }
rm -rf mpc-$MPC_VERSION   && tar xfz mpc-$MPC_VERSION.tar.gz    || { exit 1; }
rm -rf mpfr-$MPFR_VERSION && tar xfj mpfr-$MPFR_VERSION.tar.bz2 || { exit 1; }

# Link to sources from gcc source folder
cd gcc
ln -fs ../gmp-$GMP_VERSION gmp
ln -fs ../mpc-$MPC_VERSION mpc
ln -fs ../mpfr-$MPFR_VERSION mpfr

cd ../../..

#!/bin/bash

# Restore zlib
cd libs/zlib/
git checkout -- zconf.h
rm -f zconf.h.included
cd ../../../..
